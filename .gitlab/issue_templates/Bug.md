### Context

### Tasks

* [ ]  Create a component to assign members to a team. `4h`
* [ ]  Create a table to list all the teams. `1h`
* [ ]  Setup filter component to allow filter the table team for Name, team leader, status, and created date. `1h` 

### Database schema. (if applies)

### Validations
* [ ] At least one team leader is mandatory to create a team.

### Screenshots or prototypes

### English Texts

package interfaces;

public interface IStringable {

    public String toString();
}

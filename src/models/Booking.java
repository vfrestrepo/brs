package models;

import interfaces.IStringable;

public class Booking implements IStringable {

    private String from;
    private String to;
    private String date;
    private String time;
    private Bus bus;
    private Passenger passenger;
    private boolean confirmated;

    private static int quantity = 0;
    public static final int SIZE = 100;

    public Booking(String from, String to, String date, String time, Bus bus, Passenger passenger) {
        this.from = from;
        this.to = to;
        this.date = date;
        this.time = time;
        this.bus = bus;
        this.passenger = passenger;
        this.confirmated = false;
        quantity++;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Bus getBus() {
        return bus;
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    @Override
    public String toString() {
        return "TODO: ";
    }

    static int getQuantity() {
        return quantity;
    }
    
    

    public Passenger getPassenger() {
        return passenger;
    }
}

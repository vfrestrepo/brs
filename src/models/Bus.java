package models;

import interfaces.IStringable;

import javax.swing.*;

public class Bus implements IStringable {

    private String plate;
    private String type;
    private String color;
    private Person[][] seats;
    private Company company;
    private Driver driver;
    private Assistant assistant;
    private static int quantity = 0;
    public static final int SIZE = 10;

    /**
     * Only available since [1][0] to [19][1] [19][2]
     *
     * @param plate     Bus plate code
     * @param color     Bus color
     * @param company   Company to it belong
     * @param driver    Person to drive the bus
     * @param assistant Person to assist the driver
     */
    public Bus(String plate, String color, Company company, Driver driver, Assistant assistant) {
        this.plate = plate;
        this.color = color;
        this.company = company;
        this.driver = driver;
        this.assistant = assistant;
        this.seats = new Person[10][4];
        this.seats[0][0] = driver;
        this.seats[0][1] = driver;
        this.seats[0][2] = assistant;
        this.seats[0][3] = assistant;
        this.seats[9][0] = assistant;
        this.seats[9][3] = assistant;
        quantity++;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Person[][] getSeats() {
        return seats;
    }

    public void setSeats(Passenger[][] seats) {
        this.seats = seats;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Assistant getAssistant() {
        return assistant;
    }

    public void setAssistant(Assistant assistant) {
        this.assistant = assistant;
    }

    private boolean checkSeat(int row, int col) {
        return this.seats[row][col] == null;
    }

    public void addSeat(Passenger passenger, int row, int col) {
        if (!isFirstReservation(passenger.getCode())) {
            JOptionPane.showMessageDialog(null, "Do you already have a reservation", "Attention", JOptionPane.WARNING_MESSAGE);
            return;
        }
        if (checkSeat(row, col)) {
            this.seats[row][col] = passenger;
        } else {
            JOptionPane.showMessageDialog(null, "this seat is busy", "Attention", JOptionPane.WARNING_MESSAGE);
        }
    }

    public void addSeat(Passenger passenger) {
        int[] pos = positionSearcher();
        if (pos[0] == 100) {
            JOptionPane.showMessageDialog(null, "Don't have seats", "Attention", JOptionPane.WARNING_MESSAGE);
            return;
        }

        if (!isFirstReservation(passenger.getCode())) {
            JOptionPane.showMessageDialog(null, "Do you already have a reservation", "Attention", JOptionPane.WARNING_MESSAGE);
            return;
        }

        this.seats[pos[0]][pos[1]] = passenger;
    }

    private boolean isFirstReservation(String code) {
        boolean founded = true;
        exitLoop:
        for (Person[] seat : this.seats) {
            for (Person person : seat) {
                if (person != null && person.getCode().equals(code)) {
                    founded = false;
                    break exitLoop;
                }
            }
        }
        return founded;
    }

    private int[] positionSearcher() {
        int[] pos = new int[2];
        pos[0] = 100;
        outerLoop:
        for (int row = 0; row < this.seats.length; row++) {
            for (int col = 0; col < this.seats[row].length; col++) {
                if (checkSeat(row, col)) {
                    pos[0] = row;
                    pos[1] = col;
                    break outerLoop;
                }
            }
        }
        System.out.println(pos[0]);
        return pos;
    }

    @Override
    public String toString() {
        return this.plate;
    }

    public static int getQuantity() {
        return quantity;
    }

    public void cancelSeat(int row, int col) {
        this.seats[row][col] = null;

    }

}

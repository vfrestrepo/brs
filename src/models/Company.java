package models;

import interfaces.IStringable;

public class Company implements IStringable {

    private String name;
    private String code;
    private String address;
    private String phone;

    public Company(String name, String code, String address, String phone) {
        this.name = name;
        this.code = code;
        this.address = address;
        this.phone = phone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return getCode() + " - " + getName();
    }
}

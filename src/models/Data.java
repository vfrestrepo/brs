package models;

import utils.Log;

import java.util.Arrays;
import java.util.Optional;

public class Data {

    private Assistant[] assistants;
    private Bus[] buses;
    private Company company;
    private Booking[] bookings;
    private Driver[] drivers;
    private Passenger[] passengers;
    private Log log;

    public Data() {
        this.assistants = new Assistant[Assistant.SIZE];
        this.buses = new Bus[Bus.SIZE];
        this.company = new Company("ACME", "11111", "Some", "5555555");
        this.bookings = new Booking[Booking.SIZE];
        this.drivers = new Driver[Driver.SIZE];
        this.passengers = new Passenger[Passenger.SIZE];
        this.log = new Log();
        this.assistants[0] = new Assistant("Pedro", "Picapiedra", "12345", "55555", 300, "Rocadura");
        this.drivers[0] = new Driver("Pablo", "Marmol", "98765", "00000", 300, "Rocadura");
    }

    public void setAssistant(Assistant assistant) {
        this.assistants[Assistant.getQuantity() - 1] = assistant;
    }

    public void setBus(Bus bus) {
        this.buses[Bus.getQuantity() - 1] = bus;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    public void setBooking(Booking booking) {
        this.bookings[Booking.getQuantity() - 1] = booking;
    }

    public void setDriver(Driver driver) {
        this.drivers[Driver.getQuantity() - 1] = driver;
    }

    public void setPassengers(Passenger passenger) {
        this.passengers[Passenger.getQuantity() - 1] = passenger;
    }

    public Assistant[] getAssistants() {
        return assistants;
    }

    public Bus[] getBuses() {
        return buses;
    }

    public Company getCompany() {
        return company;
    }

    public Booking[] getBookings() {
        return bookings;
    }

    public Driver[] getDrivers() {
        return drivers;
    }

    public Passenger[] getPassengers() {
        return passengers;
    }

    public Optional<Passenger> filterPersonByCode(String value) {

        return Arrays.stream(passengers).filter(person -> person.getCode().equals(value)).findFirst();

    }

    public Optional<Bus> filterBusByPlate(String plate) {

        return Arrays.stream(buses).filter(bus -> bus.getPlate().equals(plate)).findFirst();

    }

    public Optional<Booking> filterBus(String plate) {

        return Arrays.stream(bookings).filter(booking -> booking.getBus().getPlate().equals(plate)).findFirst();

    }

    public Booking filterPassenger(String code) {
        Booking b = null;
        for (Booking booking : bookings) {
            if (booking != null && booking.getPassenger().getCode().equals(code)) {
                b = booking;
                break;
            }
        }
        return b;

    }


}

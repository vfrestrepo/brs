package models;

public class Driver extends Person {

    private static int quantity = 0;
    public static final int SIZE = 10;
    private String address;

    public Driver(String name, String lastName, String code, String phone, int age, String address) {
        super(name, lastName, code, phone, age);
        this.address = address;
        quantity++;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public static int getQuantity() {
        return quantity;
    }
}

package models;

public class Passenger extends Person {

    private static int quantity = 0;
    public static final int SIZE = 500;
    private String rh;

    public Passenger(String name, String lastName, String code, String phone, int age, String rh) {
        super(name, lastName, code, phone, age);
        this.rh = rh;
        quantity++;
    }

    public String getRh() {
        return rh;
    }

    public void setRh(String rh) {
        this.rh = rh;
    }

    public static int getQuantity() {
        return quantity;
    }
}

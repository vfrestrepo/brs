package models;

import interfaces.IStringable;

public class Person implements IStringable {

    private String name;
    private String lastName;
    private String code;
    private String phone;
    private int age;

    Person(String name, String lastName, String code, String phone, int age) {
        this.name = name;
        this.lastName = lastName;
        this.code = code;
        this.phone = phone;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getlastName() {
        return lastName;
    }

    public void setlastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return getCode() + " - " + getName() + " - " + getlastName();
    }
}

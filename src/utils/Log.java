package utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class Log {
    private  Logger logger = Logger.getLogger("General");

    public void print(String msg){
        logger.log(Level.INFO, msg);
    }
}

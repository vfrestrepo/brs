/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import models.Assistant;
import models.Bus;
import models.Company;
import models.Data;
import models.Driver;

/**
 *
 * @author fabio
 */
public class BusForm extends javax.swing.JFrame {

    Bus[] buses = new Bus[Bus.SIZE];
    Data Model;
    AssistantForm assistantForm;
    DriverForm driverForm;
    Booking booking;

    /**
     * Creates new form BusForm
     */
    BusForm() {
        initComponents();
    }

    BusForm(Data model, AssistantForm af, DriverForm df, Booking bookinForm) {
        this.assistantForm = af;
        this.driverForm = df;
        this.Model = model;
        this.booking = bookinForm;
        initComponents();
    }

    public void showForm() {
        Assistant assistant[] = this.Model.getAssistants();
        Driver driver[] = this.Model.getDrivers();

        clearAllCombos();

        this.setVisible(true);
        Company company = this.Model.getCompany();
        this.inputComboCompany.addItem(company.getName());

        new Thread(() -> {
            int lenghtDriver = Driver.getQuantity();
            if (lenghtDriver > 0) {
                System.out.println("len: " + lenghtDriver);
                for (int i = 0; i < lenghtDriver; i++) {
                    System.out.println("i: " + i);
                    inputComboDriver.addItem(driver[i].toString());
                }
            }
        }).start();

        new Thread(() -> {
            int lenghtAssistant = Assistant.getQuantity();
            System.out.println("len: " + lenghtAssistant);
            for (int i = 0; i < lenghtAssistant; i++) {
                System.out.println("i: " + i);
                inputComboAssistant.addItem(assistant[i].toString());
            }
        }).start();

    }

    private void clearAllCombos() {
        this.inputComboCompany.removeAllItems();
        this.inputComboAssistant.removeAllItems();
        this.inputComboDriver.removeAllItems();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        TitleBus = new javax.swing.JLabel();
        labelPlate = new javax.swing.JLabel();
        inputPlate = new javax.swing.JFormattedTextField();
        labelColo = new javax.swing.JLabel();
        inputComboColor = new javax.swing.JComboBox<>();
        labelCompany = new javax.swing.JLabel();
        inputComboCompany = new javax.swing.JComboBox<>();
        labelDriver = new javax.swing.JLabel();
        inputComboDriver = new javax.swing.JComboBox<>();
        labelAssistant = new javax.swing.JLabel();
        inputComboAssistant = new javax.swing.JComboBox<>();
        SaveBusButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        TitleBus.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        TitleBus.setText("BUS");

        labelPlate.setText("Plate");

        inputPlate.setColumns(20);

        labelColo.setText("Color");

        inputComboColor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "White", "Black", "Red", "Blue", " " }));

        labelCompany.setText("Company");

        inputComboCompany.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None" }));

        labelDriver.setText("Driver");

        inputComboDriver.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None" }));

        labelAssistant.setText("Assistant");

        inputComboAssistant.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None" }));

        SaveBusButton.setText("SAVE");
        SaveBusButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SaveBusButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(406, 406, 406)
                        .addComponent(TitleBus))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(52, 52, 52)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(labelPlate)
                                .addComponent(labelColo))
                            .addComponent(labelCompany)
                            .addComponent(labelDriver)
                            .addComponent(labelAssistant))
                        .addGap(204, 204, 204)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(inputPlate)
                            .addComponent(inputComboColor, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inputComboCompany, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inputComboDriver, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(inputComboAssistant, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(406, 406, 406)
                        .addComponent(SaveBusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(304, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addComponent(TitleBus)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelPlate)
                    .addComponent(inputPlate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelColo)
                    .addComponent(inputComboColor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelCompany)
                    .addComponent(inputComboCompany, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelDriver)
                    .addComponent(inputComboDriver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(labelAssistant)
                    .addComponent(inputComboAssistant, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 102, Short.MAX_VALUE)
                .addComponent(SaveBusButton, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void SaveBusButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SaveBusButtonActionPerformed
        String plate = this.inputPlate.getText();
        String color = this.inputComboColor.getSelectedItem().toString();
        Company company = this.Model.getCompany();
        Driver driver = this.Model.getDrivers()[this.inputComboDriver.getSelectedIndex()];
        Assistant assistant = this.Model.getAssistants()[this.inputComboAssistant.getSelectedIndex()];

        Bus bus = new Bus(plate, color, company, driver, assistant);

        this.Model.setBus(bus);
        clearForm();
        this.setVisible(false);
        this.booking.reloadComboBus();


    }//GEN-LAST:event_SaveBusButtonActionPerformed

    private void clearForm() {
        this.inputPlate.setText("");
        this.inputComboColor.setSelectedIndex(0);
        this.inputComboDriver.setSelectedIndex(0);
        this.inputComboAssistant.setSelectedIndex(0);

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BusForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> new BusForm().setVisible(true));
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton SaveBusButton;
    private javax.swing.JLabel TitleBus;
    private javax.swing.JComboBox<String> inputComboAssistant;
    private javax.swing.JComboBox<String> inputComboColor;
    private javax.swing.JComboBox<String> inputComboCompany;
    private javax.swing.JComboBox<String> inputComboDriver;
    private javax.swing.JFormattedTextField inputPlate;
    private javax.swing.JLabel labelAssistant;
    private javax.swing.JLabel labelColo;
    private javax.swing.JLabel labelCompany;
    private javax.swing.JLabel labelDriver;
    private javax.swing.JLabel labelPlate;
    // End of variables declaration//GEN-END:variables
}
